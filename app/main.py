from flask import Flask

app = Flask(__name__)

APP_PREFIX = '/api'


@app.route(APP_PREFIX + '/', methods=['GET'])
def hello_world():
    return 'Hello world'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)