FROM python:3.6.6-slim

COPY /app ~/app
COPY requirements.txt ~/app
WORKDIR ~/app

RUN pip3 install -r requirements.txt

CMD gunicorn --bind 0.0.0.0:$PORT --workers 3 wsgi:app